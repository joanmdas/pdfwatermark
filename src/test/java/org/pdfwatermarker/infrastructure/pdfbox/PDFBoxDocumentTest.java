package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.*;

public class PDFBoxDocumentTest {

    private static final String ORIGINAL_SINGLE_PAGE_PDF_NAME = "test.pdf";

    private static PDFBoxDocument pdfBoxDocument;

    @BeforeClass
    public static void beforeClass() throws Exception {
        URL urlFile = PDFBoxDocumentTest.class.getClassLoader().getResource(ORIGINAL_SINGLE_PAGE_PDF_NAME);
        assertNotNull(urlFile);
        File loadedFile = new File(urlFile.getPath());
        pdfBoxDocument= new PDFBoxDocument(loadedFile);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        pdfBoxDocument.getPDDocument().close();
    }

    @Test(expected = WatermarkPDFException.class)
    public void loadNullFile() throws WatermarkPDFException {
        new PDFBoxDocument(null);
    }

    @Test(expected = WatermarkPDFException.class)
    public void loadNonExistentFile() throws WatermarkPDFException {
        new PDFBoxDocument(new File("Something"));
    }

    @Test
    public void getPDDocument() {
        PDDocument pdDocument = pdfBoxDocument.getPDDocument();

        assertNotNull(pdDocument);
        assertEquals(1, pdDocument.getNumberOfPages());
    }

    @Test
    public void isPageOutsideOfRangeFalse() {
        boolean isOutsideOfRange = pdfBoxDocument.isPageOutsideOfRange(1);

        assertFalse(isOutsideOfRange);
    }

    @Test
    public void isPageOutsideOfRangeTrue() {
        boolean isOutsideOfRange = pdfBoxDocument.isPageOutsideOfRange(2);

        assertTrue(isOutsideOfRange);
    }

    @Test
    public void getPageMediaBox() {
        PDRectangle pdRectangle = pdfBoxDocument.getPageMediaBox(1);

        assertNotNull(pdRectangle);
        assertEquals(792f, pdRectangle.getHeight(), 0f);
        assertEquals(612f, pdRectangle.getWidth(), 0f);
    }
}