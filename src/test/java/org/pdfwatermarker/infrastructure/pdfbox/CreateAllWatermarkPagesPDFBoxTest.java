package org.pdfwatermarker.infrastructure.pdfbox;

import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.builder.CreateWatermarkInformationBuilder;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateAllWatermarkPagesPDFBoxTest {

    private static final String ORIGINAL_SINGLE_PAGE_PDF_NAME = "test.pdf";
    private static final String ORIGINAL_MULTIPLE_PAGES_PDF_NAME = "test-multipage.pdf";

    private static CreateSingleWatermarkPagePDFBox createSingleWatermarkPagePDFBox;

    private static CreateWatermarkInformation createWatermarkInformationSinglePage;
    private static CreateWatermarkInformation createWatermarkInformationMultiPage;
    private static CreateWatermarkInformation createWatermarkInformationSinglePageOutsideOfRange;

    @BeforeClass
    public static void beforeClass() {
        createSingleWatermarkPagePDFBox = mock(CreateSingleWatermarkPagePDFBox.class);

        List<Integer> singlePageList = new ArrayList<>();
        singlePageList.add(1);

        List<Integer> multiPageList = new ArrayList<>();
        multiPageList.add(2);
        multiPageList.add(5);

        URL urlFileSinglePage = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_SINGLE_PAGE_PDF_NAME);
        assertNotNull(urlFileSinglePage);

        URL urlFileMultiPage = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_MULTIPLE_PAGES_PDF_NAME);
        assertNotNull(urlFileMultiPage);

        createWatermarkInformationSinglePageOutsideOfRange = new CreateWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setPagesToWatermark(multiPageList)
                .createCreateWatermarkInformation();

        createWatermarkInformationSinglePage = new CreateWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setPagesToWatermark(singlePageList)
                .createCreateWatermarkInformation();

        createWatermarkInformationMultiPage = new CreateWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileMultiPage.getPath())
                .setPagesToWatermark(multiPageList)
                .createCreateWatermarkInformation();
    }

    @Test(expected = WatermarkPDFException.class)
    public void createPagesOutsideOfRange() throws WatermarkPDFException {
        CreateAllWatermarkPagesPDFBox createAllWatermarkPagesPDFBox =
                new CreateAllWatermarkPagesPDFBox(createSingleWatermarkPagePDFBox);
        createAllWatermarkPagesPDFBox.createPages(createWatermarkInformationSinglePageOutsideOfRange);
    }

    @Test
    public void createSinglePageWatermark() throws WatermarkPDFException {
        when(createSingleWatermarkPagePDFBox.createWatermarkPage(any(PDFBoxDocument.class),
                any(CreateWatermarkInformation.class), eq(1))).thenReturn("WatermarkPage");
        CreateAllWatermarkPagesPDFBox createAllWatermarkPagesPDFBox =
                new CreateAllWatermarkPagesPDFBox(createSingleWatermarkPagePDFBox);
        Map<Integer, String> watermarkPages = createAllWatermarkPagesPDFBox
                .createPages(createWatermarkInformationSinglePage);

        assertNotNull(watermarkPages);
        assertEquals(1, watermarkPages.size());
        assertTrue(watermarkPages.containsKey(1));
        assertTrue(watermarkPages.containsValue("WatermarkPage"));
    }

    @Test
    public void createMultiplePagesWatermark() throws WatermarkPDFException {
        when(createSingleWatermarkPagePDFBox.createWatermarkPage(any(PDFBoxDocument.class),
                any(CreateWatermarkInformation.class), eq(2))).thenReturn("WatermarkPage2");
        when(createSingleWatermarkPagePDFBox.createWatermarkPage(any(PDFBoxDocument.class),
                any(CreateWatermarkInformation.class), eq(5))).thenReturn("WatermarkPage5");
        CreateAllWatermarkPagesPDFBox createAllWatermarkPagesPDFBox =
                new CreateAllWatermarkPagesPDFBox(createSingleWatermarkPagePDFBox);
        Map<Integer, String> watermarkPages = createAllWatermarkPagesPDFBox
                .createPages(createWatermarkInformationMultiPage);

        assertNotNull(watermarkPages);
        assertEquals(2, watermarkPages.size());
        assertTrue(watermarkPages.containsKey(2));
        assertEquals("WatermarkPage2", watermarkPages.get(2));
        assertTrue(watermarkPages.containsKey(5));
        assertEquals("WatermarkPage5", watermarkPages.get(5));
    }
}