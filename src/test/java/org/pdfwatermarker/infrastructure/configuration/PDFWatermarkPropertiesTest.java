package org.pdfwatermarker.infrastructure.configuration;

import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import static org.junit.Assert.*;

public class PDFWatermarkPropertiesTest {

    @Test(expected = WatermarkPDFException.class)
    public void load() throws WatermarkPDFException {
        PDFWatermarkProperties.load(null);
    }

    @Test
    public void getPDFLibrary() {
        assertEquals(PDFWatermarkProperties.DEFAULT_PDF_LIBRARY, PDFWatermarkProperties.getPDFLibrary());
    }

    @Test
    public void getFileRepository() {
        assertEquals(PDFWatermarkProperties.DEFAULT_FILE_REPOSITORY, PDFWatermarkProperties.getFileRepository());
    }
}