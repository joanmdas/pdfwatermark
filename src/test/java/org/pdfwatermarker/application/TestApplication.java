package org.pdfwatermarker.application;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.pdfwatermarker.application.builder.PDFWatermarkRequestBuilder;
import org.pdfwatermarker.application.request.OverlayPositionEnumRequest;
import org.pdfwatermarker.application.request.PDFWatermarkRequest;
import org.pdfwatermarker.application.request.WatermarkImageSizeRequest;
import org.pdfwatermarker.application.request.WatermarkPositionEnumRequest;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.model.AddWatermarkToPDF;
import org.pdfwatermarker.domain.model.CheckRequestData;
import org.pdfwatermarker.domain.model.CreateWatermarkPages;
import org.pdfwatermarker.domain.model.DeletePDFWatermarkFiles;
import org.pdfwatermarker.infrastructure.configuration.PDFWatermarkProperties;
import org.pdfwatermarker.infrastructure.factory.FileRepositoryFactory;
import org.pdfwatermarker.infrastructure.factory.PDFOperationsRepositoryFactory;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class TestApplication {

    private static PDFWatermarkRequest pdfWatermarkRequest;

    private static Path watermarkDirectory;

    @BeforeClass
    public static void beforeClass() throws Exception {
        URL resource = TestApplication.class.getClassLoader().getResource("test-multipage.pdf");
        assertNotNull(resource);

        watermarkDirectory = Files.createTempDirectory("watermark");
        String watermarkedPDFPath = watermarkDirectory.toString() + "/IntroWatermarked.pdf";

        List<Integer> pagesToWatermark = new ArrayList<>();
        pagesToWatermark.add(1);
        pagesToWatermark.add(3);

        URL resourceStar = TestApplication.class.getClassLoader().getResource("star.png");
        assertNotNull(resourceStar);

        WatermarkImageSizeRequest watermarkImageSize = new WatermarkImageSizeRequest(200, 200);

        pdfWatermarkRequest = new PDFWatermarkRequestBuilder()
                .setOriginalPDFPath(resource.toURI().getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setPagesToWatermark(pagesToWatermark)
                .setImageWatermarkPath(resourceStar.getPath())
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnumRequest.TOP_RIGHT)
                .setOverlayPositionEnum(OverlayPositionEnumRequest.BACKGROUND)
                .createPDFWatermarkDTO();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        Files.deleteIfExists(Paths.get(pdfWatermarkRequest.getWatermarkedPDFPath()));
        Files.deleteIfExists(watermarkDirectory);
    }

    @Test
    public void execute() throws WatermarkPDFException {
        //Load properties
        Map<String, String> propertiesMap = new HashMap<>();
        propertiesMap.put(PDFWatermarkProperties.KEY_PDF_LIBRARY, PDFWatermarkProperties.DEFAULT_PDF_LIBRARY);
        propertiesMap.put(PDFWatermarkProperties.KEY_FILE_REPOSITORY, PDFWatermarkProperties.DEFAULT_FILE_REPOSITORY);
        PDFWatermarkProperties.load(propertiesMap);

        //Get instances
        CreateWatermarkPages createWatermarkPages = PDFOperationsRepositoryFactory.getInstanceCreate();
        AddWatermarkToPDF addWatermarkToPDF = PDFOperationsRepositoryFactory.getInstanceAdd();
        CheckRequestData checkRequestData = FileRepositoryFactory.getInstanceCheckRequest();
        DeletePDFWatermarkFiles deletePDFWatermarkFiles = FileRepositoryFactory.getInstanceDelete();

        //Execute
        CreatePDFWatermark createPdfWatermark = new CreatePDFWatermark(createWatermarkPages, addWatermarkToPDF, checkRequestData, deletePDFWatermarkFiles);
        createPdfWatermark.execute(pdfWatermarkRequest);

        //Asserts
        assertTrue(Files.exists(Paths.get(pdfWatermarkRequest.getWatermarkedPDFPath())));
    }
}
