package org.pdfwatermarker.domain.model;

import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.builder.CreateWatermarkInformationBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateWatermarkPagesTest {

    private static PDFOperationsRepository pdfOperationsRepository;

    @BeforeClass
    public static void beforeClass() {
        pdfOperationsRepository = mock(PDFOperationsRepository.class);
    }

    @Test
    public void execute() throws WatermarkPDFException {
        when(pdfOperationsRepository.createPages(any(CreateWatermarkInformation.class)))
                .thenReturn(new HashMap<>());

        CreateWatermarkInformation createWatermarkInformation = new CreateWatermarkInformationBuilder()
                .createCreateWatermarkInformation();

        CreateWatermarkPages createWatermarkPages = new CreateWatermarkPages(pdfOperationsRepository);

        Map<Integer, String> executeResult = createWatermarkPages.execute(createWatermarkInformation);

        assertNotNull(executeResult);
    }
}