package org.pdfwatermarker.application.request;

public enum WatermarkPositionEnumRequest {
    CENTER,
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT
}
