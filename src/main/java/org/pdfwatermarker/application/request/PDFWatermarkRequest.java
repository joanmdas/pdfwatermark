package org.pdfwatermarker.application.request;

import java.util.List;

public class PDFWatermarkRequest {
    //Path original file
    private String originalPDFPath;
    //Path watermarked file
    private String watermarkedPDFPath;
    //List of pages to watermark
    private List<Integer> pagesToWatermark;

    //Watermark information
    private String imageWatermarkPath;
    private WatermarkImageSizeRequest watermarkImageSize;
    private WatermarkPositionEnumRequest watermarkPositionEnum;
    private OverlayPositionEnumRequest overlayPositionEnum;

    public PDFWatermarkRequest() {
        //Default constructor
    }

    public PDFWatermarkRequest(String originalPDFPath, String watermarkedPDFPath, List<Integer> pagesToWatermark, String imageWatermarkPath, WatermarkImageSizeRequest watermarkImageSize,
            WatermarkPositionEnumRequest watermarkPositionEnum, OverlayPositionEnumRequest overlayPositionEnum) {
        this.originalPDFPath = originalPDFPath;
        this.watermarkedPDFPath = watermarkedPDFPath;
        this.pagesToWatermark = pagesToWatermark;
        this.imageWatermarkPath = imageWatermarkPath;
        this.watermarkImageSize = watermarkImageSize;
        this.watermarkPositionEnum = watermarkPositionEnum;
        this.overlayPositionEnum = overlayPositionEnum;
    }

    public String getOriginalPDFPath() {
        return this.originalPDFPath;
    }

    public void setOriginalPDFPath(String originalPDFPath) {
        this.originalPDFPath = originalPDFPath;
    }

    public String getWatermarkedPDFPath() {
        return this.watermarkedPDFPath;
    }

    public void setWatermarkedPDFPath(String watermarkedPDFPath) {
        this.watermarkedPDFPath = watermarkedPDFPath;
    }

    public List<Integer> getPagesToWatermark() {
        return this.pagesToWatermark;
    }

    public void setPagesToWatermark(List<Integer> pagesToWatermark) {
        this.pagesToWatermark = pagesToWatermark;
    }

    public String getImageWatermarkPath() {
        return this.imageWatermarkPath;
    }

    public void setImageWatermarkPath(String imageWatermarkPath) {
        this.imageWatermarkPath = imageWatermarkPath;
    }

    public WatermarkImageSizeRequest getWatermarkImageSize() {
        return this.watermarkImageSize;
    }

    public void setWatermarkImageSize(WatermarkImageSizeRequest watermarkImageSize) {
        this.watermarkImageSize = watermarkImageSize;
    }

    public WatermarkPositionEnumRequest getWatermarkPositionEnum() {
        return this.watermarkPositionEnum;
    }

    public void setWatermarkPositionEnum(WatermarkPositionEnumRequest watermarkPositionEnum) {
        this.watermarkPositionEnum = watermarkPositionEnum;
    }

    public OverlayPositionEnumRequest getOverlayPositionEnum() {
        return this.overlayPositionEnum;
    }

    public void setOverlayPositionEnum(OverlayPositionEnumRequest overlayPositionEnum) {
        this.overlayPositionEnum = overlayPositionEnum;
    }
}
