package org.pdfwatermarker.application.builder;

import org.pdfwatermarker.application.request.OverlayPositionEnumRequest;
import org.pdfwatermarker.application.request.PDFWatermarkRequest;
import org.pdfwatermarker.application.request.WatermarkImageSizeRequest;
import org.pdfwatermarker.application.request.WatermarkPositionEnumRequest;

import java.util.List;

public class PDFWatermarkRequestBuilder {
    private String originalPDFPath;
    private String watermarkedPDFPath;
    private List<Integer> pagesToWatermark;
    private String imageWatermarkPath;
    private WatermarkImageSizeRequest watermarkImageSize;
    private WatermarkPositionEnumRequest watermarkPositionEnum;
    private OverlayPositionEnumRequest overlayPositionEnum;

    public PDFWatermarkRequestBuilder setOriginalPDFPath(String originalPDFPath) {
        this.originalPDFPath = originalPDFPath;
        return this;
    }

    public PDFWatermarkRequestBuilder setWatermarkedPDFPath(String watermarkedPDFPath) {
        this.watermarkedPDFPath = watermarkedPDFPath;
        return this;
    }

    public PDFWatermarkRequestBuilder setPagesToWatermark(List<Integer> pagesToWatermark) {
        this.pagesToWatermark = pagesToWatermark;
        return this;
    }

    public PDFWatermarkRequestBuilder setImageWatermarkPath(String imageWatermarkPath) {
        this.imageWatermarkPath = imageWatermarkPath;
        return this;
    }

    public PDFWatermarkRequestBuilder setWatermarkImageSize(WatermarkImageSizeRequest watermarkImageSize) {
        this.watermarkImageSize = watermarkImageSize;
        return this;
    }

    public PDFWatermarkRequestBuilder setWatermarkPositionEnum(WatermarkPositionEnumRequest watermarkPositionEnum) {
        this.watermarkPositionEnum = watermarkPositionEnum;
        return this;
    }

    public PDFWatermarkRequestBuilder setOverlayPositionEnum(OverlayPositionEnumRequest overlayPositionEnum) {
        this.overlayPositionEnum = overlayPositionEnum;
        return this;
    }

    public PDFWatermarkRequest createPDFWatermarkDTO() {
        return new PDFWatermarkRequest(originalPDFPath, watermarkedPDFPath, pagesToWatermark, imageWatermarkPath,
                watermarkImageSize, watermarkPositionEnum, overlayPositionEnum);
    }
}