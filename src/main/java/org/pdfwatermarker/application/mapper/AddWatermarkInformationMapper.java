package org.pdfwatermarker.application.mapper;

import org.pdfwatermarker.application.request.OverlayPositionEnumRequest;
import org.pdfwatermarker.application.request.PDFWatermarkRequest;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.WatermarkOverlayPosition;
import org.pdfwatermarker.domain.valueobjects.builder.AddWatermarkInformationBuilder;

import java.util.Map;

public class AddWatermarkInformationMapper {

    private AddWatermarkInformationMapper() {
        //Default private constructor
    }

    public static AddWatermarkInformation map(PDFWatermarkRequest pdfWatermarkRequest, Map<Integer, String> watermarkedPages) {
        return new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(pdfWatermarkRequest.getOriginalPDFPath())
                .setWatermarkedPDFPath(pdfWatermarkRequest.getWatermarkedPDFPath())
                .setWatermarkedPages(watermarkedPages)
                .setWatermarkOverlayPosition(mapOverlayPosition(pdfWatermarkRequest.getOverlayPositionEnum()))
                .createAddWatermarkInformation();
    }

    private static WatermarkOverlayPosition mapOverlayPosition(OverlayPositionEnumRequest overlayPositionEnumRequest) {
        return WatermarkOverlayPosition.valueOf(overlayPositionEnumRequest.toString());
    }
}
