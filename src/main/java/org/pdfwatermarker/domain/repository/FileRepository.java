package org.pdfwatermarker.domain.repository;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import java.io.File;
import java.util.Map;

public interface FileRepository {

    void checkFileExist(String path) throws WatermarkPDFException;

    void checkFolderExist(String path) throws WatermarkPDFException;

    File createTmpFile() throws WatermarkPDFException;

    void deletePDFWatermarkFiles(Map<Integer, String> watermarkedPages) throws WatermarkPDFException;
}
