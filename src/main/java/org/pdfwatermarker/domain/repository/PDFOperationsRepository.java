package org.pdfwatermarker.domain.repository;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;

import java.util.Map;

public interface PDFOperationsRepository {

    Map<Integer, String> createPages(CreateWatermarkInformation createWatermarkInformation) throws WatermarkPDFException;

    void addWatermark(AddWatermarkInformation addWatermarkInformation) throws WatermarkPDFException;

}
