package org.pdfwatermarker.domain.valueobjects;

import java.util.Map;

public class AddWatermarkInformation {
    //Path original file
    private final String originalPDFPath;
    //Path watermarked file
    private final String watermarkedPDFPath;
    //Watermarked pages list
    private final Map<Integer, String> watermarkedPages;
    //Overlay position enum
    private final WatermarkOverlayPosition watermarkOverlayPosition;

    public AddWatermarkInformation(String originalPDFPath, String watermarkedPDFPath,
                                   Map<Integer, String> watermarkedPages,
                                   WatermarkOverlayPosition watermarkOverlayPosition) {
        this.originalPDFPath = originalPDFPath;
        this.watermarkedPDFPath = watermarkedPDFPath;
        this.watermarkedPages = watermarkedPages;
        this.watermarkOverlayPosition = watermarkOverlayPosition;
    }

    public String getOriginalPDFPath() {
        return originalPDFPath;
    }

    public String getWatermarkedPDFPath() {
        return watermarkedPDFPath;
    }

    public Map<Integer, String> getWatermarkedPages() {
        return watermarkedPages;
    }

    public WatermarkOverlayPosition getWatermarkOverlayPosition() {
        return watermarkOverlayPosition;
    }
}
