package org.pdfwatermarker.domain.valueobjects;

public enum WatermarkPositionEnum {
    CENTER,
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT
}
