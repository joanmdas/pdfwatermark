package org.pdfwatermarker.domain.valueobjects;

import java.util.List;

public class CreateWatermarkInformation {
    //Path original file
    private final String originalPDFPath;
    //List of pages to watermark
    private final List<Integer> pagesToWatermark;

    //Watermark information
    private final String imageWatermarkPath;
    private final WatermarkImageSize watermarkImageSize;
    private final WatermarkPositionEnum watermarkPositionEnum;
    private final WatermarkOverlayPosition watermarkOverlayPosition;

    public CreateWatermarkInformation(String originalPDFPath, List<Integer> pagesToWatermark, String imageWatermarkPath,
                                      WatermarkImageSize watermarkImageSize, WatermarkPositionEnum watermarkPositionEnum,
                                      WatermarkOverlayPosition watermarkOverlayPosition) {
        this.originalPDFPath = originalPDFPath;
        this.pagesToWatermark = pagesToWatermark;
        this.imageWatermarkPath = imageWatermarkPath;
        this.watermarkImageSize = watermarkImageSize;
        this.watermarkPositionEnum = watermarkPositionEnum;
        this.watermarkOverlayPosition = watermarkOverlayPosition;
    }

    public String getOriginalPDFPath() {
        return originalPDFPath;
    }

    public List<Integer> getPagesToWatermark() {
        return pagesToWatermark;
    }

    public String getImageWatermarkPath() {
        return imageWatermarkPath;
    }

    public WatermarkImageSize getWatermarkImageSize() {
        return watermarkImageSize;
    }

    public WatermarkPositionEnum getWatermarkPositionEnum() {
        return watermarkPositionEnum;
    }

    public WatermarkOverlayPosition getWatermarkOverlayPosition() {
        return watermarkOverlayPosition;
    }

}
