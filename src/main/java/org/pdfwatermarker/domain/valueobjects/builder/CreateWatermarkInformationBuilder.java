package org.pdfwatermarker.domain.valueobjects.builder;

import org.pdfwatermarker.domain.valueobjects.*;

import java.util.List;

public class CreateWatermarkInformationBuilder {

    private String originalPDFPath;
    private List<Integer> pagesToWatermark;
    private String imageWatermarkPath;
    private WatermarkImageSize watermarkImageSize;
    private WatermarkPositionEnum watermarkPositionEnum;
    private WatermarkOverlayPosition watermarkOverlayPosition;

    public CreateWatermarkInformationBuilder setOriginalPDFPath(String originalPDFPath) {
        this.originalPDFPath = originalPDFPath;
        return this;
    }

    public CreateWatermarkInformationBuilder setPagesToWatermark(List<Integer> pagesToWatermark) {
        this.pagesToWatermark = pagesToWatermark;
        return this;
    }

    public CreateWatermarkInformationBuilder setImageWatermarkPath(String imageWatermarkPath) {
        this.imageWatermarkPath = imageWatermarkPath;
        return this;
    }

    public CreateWatermarkInformationBuilder setWatermarkImageSize(WatermarkImageSize watermarkImageSize) {
        this.watermarkImageSize = watermarkImageSize;
        return this;
    }

    public CreateWatermarkInformationBuilder setWatermarkPositionEnum(WatermarkPositionEnum watermarkPositionEnum) {
        this.watermarkPositionEnum = watermarkPositionEnum;
        return this;
    }

    public CreateWatermarkInformationBuilder setWatermarkOverlayPosition(WatermarkOverlayPosition watermarkOverlayPosition) {
        this.watermarkOverlayPosition = watermarkOverlayPosition;
        return this;
    }

    public CreateWatermarkInformation createCreateWatermarkInformation() {
        return new CreateWatermarkInformation(originalPDFPath, pagesToWatermark, imageWatermarkPath, watermarkImageSize, watermarkPositionEnum, watermarkOverlayPosition);
    }
}