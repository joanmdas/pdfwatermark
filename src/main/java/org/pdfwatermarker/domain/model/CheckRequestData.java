package org.pdfwatermarker.domain.model;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.FileRepository;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CheckRequestData {

    private final FileRepository fileRepository;

    public CheckRequestData(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public void checkFile(final String path) throws WatermarkPDFException {
        fileRepository.checkFileExist(path);
    }

    public void checkTargetFile(final String path) throws WatermarkPDFException {

        if (path == null || !path.endsWith(".pdf")) {
            throw new WatermarkPDFException("The file " + path + " does not exist");
        }

        Path pathToFile = Paths.get(path);
        String directory = pathToFile.getParent().toString();
        fileRepository.checkFolderExist(directory);
    }

    public void checkValidPagesToWatermark(List<Integer> pagesToWatermark) throws WatermarkPDFException {
        if (pagesToWatermark == null || pagesToWatermark.isEmpty()) {
            throw new WatermarkPDFException("The are no pages to watermark");
        }
    }
}
