package org.pdfwatermarker.domain.model;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.FileRepository;

import java.util.Map;

/**
 * Delete watermark PDF page once already used
 */
public class DeletePDFWatermarkFiles {

    private final FileRepository fileRepository;

    public DeletePDFWatermarkFiles(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public void execute(Map<Integer, String> watermarkedPages) throws WatermarkPDFException {
        this.fileRepository.deletePDFWatermarkFiles(watermarkedPages);
    }
}
