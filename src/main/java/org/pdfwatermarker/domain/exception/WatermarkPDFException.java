package org.pdfwatermarker.domain.exception;

public class WatermarkPDFException extends Exception {

    public WatermarkPDFException(String message) {
        super(message);
    }
}
