package org.pdfwatermarker.infrastructure.pdfbox;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateAllWatermarkPagesPDFBox {

    private final CreateSingleWatermarkPagePDFBox createSingleWatermarkPagePDFBox;

    public CreateAllWatermarkPagesPDFBox(CreateSingleWatermarkPagePDFBox createSingleWatermarkPagePDFBox) {
        this.createSingleWatermarkPagePDFBox = createSingleWatermarkPagePDFBox;
    }

    public Map<Integer, String> createPages(CreateWatermarkInformation createWatermarkInformation)
            throws WatermarkPDFException {

        PDFBoxDocument originalPDF = new PDFBoxDocument(new File(createWatermarkInformation.getOriginalPDFPath()));

        List<Integer> pagesToAddWatermark = createWatermarkInformation.getPagesToWatermark();

        Map<Integer, String> watermarkPdfMap = new HashMap<>();

        for (int i = 1; i <= pagesToAddWatermark.size(); i++) {
            Integer pageToWatermark = pagesToAddWatermark.get(i-1);
            if (originalPDF.isPageOutsideOfRange(pageToWatermark)) {
                throw new WatermarkPDFException("Page " + pageToWatermark + " does not exist in PDF");
            }

            String watermarkPdfFilePath = createSingleWatermarkPagePDFBox.createWatermarkPage(originalPDF, createWatermarkInformation,
                    pageToWatermark);
            watermarkPdfMap.put(pageToWatermark, watermarkPdfFilePath);
        }

        return watermarkPdfMap;
    }

}
