package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.pdfwatermarker.domain.valueobjects.WatermarkPositionCoordinates;
import org.pdfwatermarker.domain.valueobjects.WatermarkPositionEnum;

/**
 * Calculates watermark coordinates. Those coordinates are calculated depending on what the library uses as starting
 * position. In other words, what position does the library considers the coordinates (0,0) to be. In the case of PDFBox,
 * the coordinates (0,0) are in the bottom left of the page.
 */
public class CalculateWatermarkCoordinatesPDFBox {

    private final PDRectangle pageArea;
    private final PDImageXObject imageArea;

    public CalculateWatermarkCoordinatesPDFBox(PDRectangle pageArea, PDImageXObject imageArea) {
        this.pageArea = pageArea;
        this.imageArea = imageArea;
    }

    public WatermarkPositionCoordinates getCoordinates(WatermarkPositionEnum position) {

        if (position.equals(WatermarkPositionEnum.TOP_LEFT)) {
            return calculateTopLeftPosition();
        }
        else if (position.equals(WatermarkPositionEnum.TOP_RIGHT)) {
            return calculateTopRightPosition();
        }
        else if (position.equals(WatermarkPositionEnum.BOTTOM_LEFT)) {
            return calculateBottomLeftPosition();
        }
        else if (position.equals(WatermarkPositionEnum.BOTTOM_RIGHT)) {
            return calculateBottomRightPosition();
        }
        else {
            return calculateCenterPosition();
        }
    }

    private WatermarkPositionCoordinates calculateTopLeftPosition() {
        float yAxis = calculateYAxis();
        return new WatermarkPositionCoordinates(0, yAxis);
    }

    private WatermarkPositionCoordinates calculateTopRightPosition() {
        float xAxis = calculateXAxis();
        float yAxis = calculateYAxis();
        return new WatermarkPositionCoordinates(xAxis, yAxis);
    }

    private WatermarkPositionCoordinates calculateBottomLeftPosition() {
        return new WatermarkPositionCoordinates(0, 0);
    }

    private WatermarkPositionCoordinates calculateBottomRightPosition() {
        float xAxis = calculateXAxis();
        return new WatermarkPositionCoordinates(xAxis, 0);
    }

    private WatermarkPositionCoordinates calculateCenterPosition() {
        float xAxis = calculateXAxis() / 2;
        float yAxis = calculateYAxis() / 2;
        return new WatermarkPositionCoordinates(xAxis, yAxis);
    }

    private float calculateXAxis() {
        return pageArea.getWidth() - imageArea.getWidth();
    }

    private float calculateYAxis() {
        return pageArea.getHeight() - imageArea.getHeight();
    }
}
