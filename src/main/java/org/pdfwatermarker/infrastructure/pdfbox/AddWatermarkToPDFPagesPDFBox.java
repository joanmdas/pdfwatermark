package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;

import java.io.File;
import java.io.IOException;

public class AddWatermarkToPDFPagesPDFBox {

    public void addWatermark(AddWatermarkInformation addWatermarkInformation)
            throws WatermarkPDFException {

        PDFBoxDocument pdfBoxDocument = new PDFBoxDocument(new File(addWatermarkInformation.getOriginalPDFPath()));
        PDDocument originalPDF = pdfBoxDocument.getPDDocument();

        Overlay.Position overlayPosition =
                Overlay.Position.valueOf(addWatermarkInformation.getWatermarkOverlayPosition().toString());

        try (Overlay overlay = new Overlay()) {
            overlay.setInputPDF(originalPDF);
            overlay.setOverlayPosition(overlayPosition);
            overlay.overlay(addWatermarkInformation.getWatermarkedPages());
            originalPDF.save(addWatermarkInformation.getWatermarkedPDFPath());
            originalPDF.close();
        } catch (IOException e) {
            throw new WatermarkPDFException(e.getMessage());
        }
    }
}
