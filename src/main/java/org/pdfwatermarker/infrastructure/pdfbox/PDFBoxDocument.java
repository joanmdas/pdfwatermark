package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import java.io.File;
import java.io.IOException;

public class PDFBoxDocument {

    private final PDDocument document;

    public PDFBoxDocument(File file) throws WatermarkPDFException {
        if (file == null) {
            throw new WatermarkPDFException("File can't be null");
        }

        try {
            this.document = PDDocument.load(file);
        } catch (IOException e) {
            throw new WatermarkPDFException(e.getMessage());
        }
    }


    public PDDocument getPDDocument() {
        return document;
    }

    public boolean isPageOutsideOfRange(Integer pageToAddWatermark) {
        return pageToAddWatermark > document.getPages().getCount();
    }

    public PDRectangle getPageMediaBox(int page) {
        return document.getPage(page - 1).getMediaBox();
    }
}
