package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.FileRepository;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.WatermarkPositionCoordinates;

import java.io.File;
import java.io.IOException;

/**
 * Create watermark PDF page
 */
public class CreateSingleWatermarkPagePDFBox {

    private final FileRepository fileRepository;

    public CreateSingleWatermarkPagePDFBox(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public String createWatermarkPage(PDFBoxDocument originalPDF, CreateWatermarkInformation createWatermarkInformation,
                                      Integer pageToWatermark) throws WatermarkPDFException {
        PDDocument watermarkPDF = new PDDocument();

        PDImageXObject pdImage = getImageObject(createWatermarkInformation, watermarkPDF);

        drawWatermarkImage(watermarkPDF, pageToWatermark, originalPDF, pdImage, createWatermarkInformation);

        File tmpFile = saveTmpFile(watermarkPDF);

        return tmpFile.getAbsolutePath();
    }

    private PDImageXObject getImageObject(CreateWatermarkInformation createWatermarkInformation, PDDocument watermarkPDF)
            throws WatermarkPDFException {
        PDImageXObject pdImage;
        try {
            pdImage = PDImageXObject.createFromFile(createWatermarkInformation.getImageWatermarkPath(), watermarkPDF);
        } catch (IOException e) {
            throw new WatermarkPDFException(e.getMessage());
        }
        pdImage.setHeight(createWatermarkInformation.getWatermarkImageSize().getHeight());
        pdImage.setWidth(createWatermarkInformation.getWatermarkImageSize().getWidth());

        return pdImage;
    }

    private void drawWatermarkImage(PDDocument watermarkPDF,
                                    Integer pageToWatermark,
                                    PDFBoxDocument originalPDF,
                                    PDImageXObject pdImage,
                                    CreateWatermarkInformation createWatermarkInformation) throws WatermarkPDFException {

        PDPage page = new PDPage(originalPDF.getPageMediaBox(pageToWatermark));
        watermarkPDF.addPage(page);

        try (PDPageContentStream contents = new PDPageContentStream(watermarkPDF, page)) {
            PDRectangle mediaBox = page.getMediaBox();

            CalculateWatermarkCoordinatesPDFBox calculateWatermarkCoordinatesPDFBox = new CalculateWatermarkCoordinatesPDFBox(mediaBox, pdImage);
            WatermarkPositionCoordinates watermarkPositionCoordinates = calculateWatermarkCoordinatesPDFBox.getCoordinates(createWatermarkInformation.getWatermarkPositionEnum());

            contents.drawImage(pdImage, watermarkPositionCoordinates.getXAxis(), watermarkPositionCoordinates.getYAxis());
        } catch (IOException e) {
            throw new WatermarkPDFException(e.getMessage());
        }
    }

    private File saveTmpFile(PDDocument watermarkPDF) throws WatermarkPDFException {
        File tmpFile;
        try {
            tmpFile = this.fileRepository.createTmpFile();
            watermarkPDF.save(tmpFile);
            watermarkPDF.close();
        } catch (IOException e) {
            throw new WatermarkPDFException(e.getMessage());
        }

        return tmpFile;
    }
}
