package org.pdfwatermarker.infrastructure.factory;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.FileRepository;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;
import org.pdfwatermarker.infrastructure.configuration.PDFWatermarkProperties;
import org.pdfwatermarker.infrastructure.fs.FSFileRepository;
import org.pdfwatermarker.infrastructure.pdfbox.*;

public abstract class AbstractFactory {

    protected AbstractFactory() {
        //Default private constructor
    }

    protected static final String INVALID_PDF_LIBRARY = "Invalid pdf library ";
    protected static final String INVALID_FILE_REPOSITORY = "Invalid file repository ";

    protected static final String PDFBOX_LIBRARY = "pdfbox";
    protected static final String FS_REPOSITORY = "fs";

    protected static FileRepository loadFileRepository() throws WatermarkPDFException {
        FileRepository fileRepository;

        if (PDFWatermarkProperties.getFileRepository().equals(FS_REPOSITORY)) {
            fileRepository = new FSFileRepository();
        }
        else {
            throw new WatermarkPDFException(INVALID_FILE_REPOSITORY + PDFWatermarkProperties.getFileRepository());
        }

        return fileRepository;
    }

    protected static PDFOperationsRepository loadPDFOperations(FileRepository fileRepository) throws WatermarkPDFException {
        PDFOperationsRepository pdfOperationsRepository;

        if (PDFWatermarkProperties.getPDFLibrary().equals(PDFBOX_LIBRARY)) {
            CreateSingleWatermarkPagePDFBox createSingleWatermarkPagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
            CreateAllWatermarkPagesPDFBox createAllWatermarkPagesPDFBox = new CreateAllWatermarkPagesPDFBox(createSingleWatermarkPagePDFBox);
            AddWatermarkToPDFPagesPDFBox addWatermarkToPDFPagesPDFBox = new AddWatermarkToPDFPagesPDFBox();


            pdfOperationsRepository = new PDFBoxPDFOperationsRepository(createAllWatermarkPagesPDFBox, addWatermarkToPDFPagesPDFBox);
        }
        else {
            throw new WatermarkPDFException(INVALID_PDF_LIBRARY + PDFWatermarkProperties.getPDFLibrary());
        }

        return pdfOperationsRepository;
    }
}
