package org.pdfwatermarker.infrastructure.factory;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.model.CheckRequestData;
import org.pdfwatermarker.domain.model.DeletePDFWatermarkFiles;
import org.pdfwatermarker.domain.repository.FileRepository;

public class FileRepositoryFactory extends AbstractFactory{

    private FileRepositoryFactory() {
        //Default private constructor
    }

    public static CheckRequestData getInstanceCheckRequest() throws WatermarkPDFException {
        FileRepository fileRepository = loadFileRepository();
        return new CheckRequestData(fileRepository);
    }

    public static DeletePDFWatermarkFiles getInstanceDelete() throws WatermarkPDFException {
        FileRepository fileRepository = loadFileRepository();
        return new DeletePDFWatermarkFiles(fileRepository);
    }
}
