package org.pdfwatermarker.infrastructure.factory;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.model.AddWatermarkToPDF;
import org.pdfwatermarker.domain.model.CreateWatermarkPages;
import org.pdfwatermarker.domain.repository.FileRepository;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;

public class PDFOperationsRepositoryFactory extends AbstractFactory {

    public static CreateWatermarkPages getInstanceCreate() throws WatermarkPDFException {
        FileRepository fileRepository = loadFileRepository();
        PDFOperationsRepository pdfOperationsRepository = loadPDFOperations(fileRepository);

        return new CreateWatermarkPages(pdfOperationsRepository);
    }

    public static AddWatermarkToPDF getInstanceAdd() throws WatermarkPDFException {
        FileRepository fileRepository = loadFileRepository();
        PDFOperationsRepository pdfOperationsRepository = loadPDFOperations(fileRepository);

        return new AddWatermarkToPDF(pdfOperationsRepository);
    }

}
