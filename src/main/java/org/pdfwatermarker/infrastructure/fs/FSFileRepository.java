package org.pdfwatermarker.infrastructure.fs;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.FileRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class FSFileRepository implements FileRepository {

    private static final String TMP_FILE_PREFIX = "image";
    private static final String TMP_FILE_SUFFIX = ".pdf";

    public FSFileRepository() {
        //Default constructor
    }

    @Override
    public void checkFileExist(String path) throws WatermarkPDFException {
        File file = new File(path);
        if (!file.exists()) {
            throw new WatermarkPDFException("The file " + path + " does not exist");
        }
    }

    @Override
    public void checkFolderExist(String path) throws WatermarkPDFException {
        File file = new File(path);
        if (!file.exists() || !file.isDirectory()) {
            throw new WatermarkPDFException("The folder " + path + " does not exist");
        }
    }

    @Override
    public File createTmpFile() throws WatermarkPDFException {
        try {
            return File.createTempFile(TMP_FILE_PREFIX, TMP_FILE_SUFFIX);
        } catch (IOException e) {
            throw new WatermarkPDFException(e.getMessage());
        }
    }

    @Override
    public void deletePDFWatermarkFiles(Map<Integer, String> watermarkedPages) {
        watermarkedPages.forEach((k, v) -> {
            Path path = Paths.get(v);
            try {
                Files.deleteIfExists(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
