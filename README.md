# PDF Watermark 🎓🏭
# La Salle Ramon Llull – MDAS 🤖

# Contenido 📇
* **1. Infraestructura**
  * 1.1. PDFBox
  * 1.2. FileSystem
  * 1.3. Factory
  * 1.4. Configuration
* **2. Aplicación**
  * 2.1. Casos de uso
  * 2.2. Request
  * 2.3. Builder
  * 2.4. Mapper
* **3. Dominio**
  * 3.1. Modelos
  * 3.2. Repositorios
  * 3.3. Value Objects
  * 3.4. Exceptions
* **4. Conclusiones**
* **5. Instalación del proyecto**
* **6. Diagrama de classes**

# Arquitectura Hexagonal - DDD

![Hexagonal Image](/doc/hexa.png)


# 1. Infraestructura
La capa de infraestructura contiene la lógica necesaria para ejecutar la aplicación.

## 1.1. PDFBox
La biblioteca PDFBox  es una herramienta Java de código abierto para trabajar con documentos PDF. Este proyecto permite 
la creación de nuevos documentos PDF, manipulación y mucho más.

## 1.2. FileSystem
Sistema de archivos es un método para almacenar y organizar archivos de ordenador y los datos que contienen con el fin 
de facilitar su localización y acceso.

## 1.3. Factory
Factory se encarga de seleccionar la correcta implementación a usar durante la ejecución de la aplicación. La selección 
de la correcta implementación se hace mediante propiedades.

## 1.4. Configuration
Contiene las propiedades necesarias para la ejecución de la aplicación. Además te permite cambiar el valor de estas 
propiedades.

Seguramente para esta practica los factories y la configuración no harian falta ya que solo hay una implementación para 
trabajar con PDF y para el repositorio de ficheros. No obstante queriamos crear una forma de poder seleccionar que 
implementación usar mediante parametros de configuración. De esta forma abstraemos la logica de la selección de 
implementaciones a los factories.

![Usage Conf and Factories](/doc/configuration-factory-usage.png)


# 2. Aplicación
La capa de aplicación se encuentra entre el dominio y la infraestructura y permite la comunicación entre las dos capas.

## 2.1. Casos de uso
Nuestra aplicación gira entorno a un caso de uso principal, que le permite al usuario crear un PDF con una marca de agua:
* **CreatePDFWatermark**: Nos permite crear un PDF con la marca de agua selecionada. Recibe como parametros para la 
creación PDFWatermarkRequest.

## 2.2. Request
En este package guardamos los parametros de entrada que necessitara el caso de uso para poder funcionar.

## 2.3. Builder
Aplicamos el patrón builder sobre los parametros que estan en el package de request. Esta pensado para ser usado cuando se 
llame a esta libreria de la siguiente forma:

![Example Builder](/doc/example-builder.png)

## 2.4. Mapper
Se ha creado mapper para transformar la información que serà usada en los modelos de domain. De esta forma impedimos que 
haya referencias a capa de aplicación en la capa de dominio, manteniendo así la arquitectura hexagonal.


# 3. Domain
La capa de dominio es el núcleo de nuestra aplicación. Solo contendrá lógica empresarial y la aislaremos de otras capas.
* Representan los elementos de nuestro contexto.

## 3.1. Model
Aquí es donde vive la llamada lógica empresarial, donde se toman todas las decisiones.
* **CheckRequestData**: se encarga de comprobar que los datos recibidos son validos.
* **CreateWatermarkPages**: crea paginas en blanco que contienen la posición y tamaño de los watermark que se van a insertar.
* **AddWatermarkToPDF**: inserta los watermark creados al fichero pdf.
* **DeletePDFWatermarkFiles**: borra las páginas en blanco con los watermark creadas en el "CreateWatermarkPages".

## 3.2. Repository
Aquí encontramos solo los contratos (interficies) de nuestros repositorios:
* **FileRepository**: Se encarga de definir la comunicación con el sistema de ficheros. Nos permite comprobar si existe 
el fichero, la ruta, y crear y borrar ficheros temporales.
* **PDFOperationsRepository**: Se definen las operaciones que se pueden realizar sobre el fichero pdf. Define operaciones 
de  crear las páginas con la marca de agua y añadirlas al fichero pdf.

## 3.3. Value Objects
En este package encontraremos las clases que contienen la información necesaria para ejecutar la lógica de los models. 
También se encuentran los builders que usará la capa de aplicación para transformar los datos de la request a datos de 
la capa de dominio.

## 3.4. Exceptions
Aquí se encuentra la excepción que la libreria lanzará en el caso que haya algun problema durante la ejecución.

# 4. Conclusiones 🧠
Hemos podido aplicar todos los conocimientos adquiridos de principios **SOLID** y del paradigma **DDD (Domain Driver Design)**. También hemos aplicado alguno de los **patrones** mostrados en clase allí donde hacían falta, todo esto para hacer el código más **mantenible, escalable y entendible**.

Hemos aprendido a **diferenciar las capas de la arquitectura hexagonal**, así como también la **comunicación** entre cada una de estas.

También a nivel de equipo ha supuesto mejorar la **toma de decisiones** a nivel de arquitectura de software **debatiendo** entre los compañeros con **revisiones periódicas** las diferentes visiones de implementación/diseño a medida que avanzaba el proyecto, refinándolo semana tras semana aprendiendo también de la visión de cada uno de los integrantes del grupo.

# 5. Instalación/Configuración proyecto ⚙️

### Step - 1️⃣ Descargar el proyecto (clone): `git clone https://gitlab.com/joanmdas/pdfwatermark.git`📥
### Step - 2️⃣ Instalar OpenJDK 11: https://adoptopenjdk.net/ 📥
![OpenJDK ](/doc/downloadOpenJDK.png)
### Step - 3️⃣ Ejecutar el JAR (levanta la aplicación con SpringBoot Controller API Rest) 🖥️🆙
Lo ejecutaremos posicionandos en la ruta donde se encuentra el fichero (terminal) "**pdfwatermark/demo/pdfwatermarker-test-0.0.1.jar**" y ejecutando el siguiente comando: 


- `java -jar pdfwatermarker-test-0.0.1.jar`
### Step - 4️⃣ Lanzamos las peticiones POST para la creación del PDF (Postman) 📩
* Puedes importar la llamada POST-JSON que encontraras en "**pdfwatermark/demo/watermark.postman_collection.json**".
![Postman Example ](/doc/PostmanPostExample.PNG)

### Step - 5️⃣ **Disfrutar de nuestro PDF con su marca de agua!😍✨**


# 6. Diagrama de classes (Diseño del Software) ✍️

![Diagrama UML ](/doc/UML.png)
